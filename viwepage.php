<!DOCTYPE>
<html lang="ja-JP">
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<style>
    a{
        display: inline-block;
        text-decoration: none;
        padding:3px 6px;
        min-width:100%;
        color:tomato;
    }
    a:hover{
        background-color:tomato;
        color:white;

    }
    div.col-3{
        min-height:170px;
    }
    div.col-3 a{
        min-height:170px;
    }
    div.col-3 a:hover{
        color:#444;
    }
    div.col-12{
        min-height:170px;
    }
    div.col-12 a{
        background-color:#eeeeee;
        min-height:170px;
        margin-bottom:20px;
    }
    div.col-12 a:hover{
        background-color:tomato
    }
</style>
</head>
<body>
<?php
    $request = explode("/",trim(urldecode($_GET["req"]??"")));
    $request_path = implode("/",$request);
    //$path = dirname(__DIR__."/../".$request_path);
    //main use $testpath
    $path = dirname(__DIR__).$request_path;
    $dirs = scandir($path);
    function z_mb_urlencode( $str ) {
        return preg_replace_callback(
        '/[^\x21-\x7e]+/',
        function( $matches ) {
            return urlencode( $matches[0] );
        },
        $str );
    }
    $printFunc = function($dirs) use ($request,$path){
        $files_html_element = [];
        echo "<div class='my-4 container-fluid row'>";
        $page = intval($_GET["p"])??1;
        $dir_count = count($dirs);
        $counter = 1;
        foreach($dirs as $dir){
            if($dir === "."){
            }else if($dir === ".."){
                $new_request = $request;
                $new_request[] = "..";
                echo "<div class='col-12'><a href='javascript:history.go(-1)'>戻る</a></div>";
            }else{
                $new_request = $request;
                $new_request[] = end(explode("/",trim($dir)));
                clearstatcache();
                if(is_dir($path."/".$dir)){
                    if((intval($_GET["p"])??1) < 2){
                    echo "<div class='col-3 col-md-4 col-sm-12'><a href='".
                    "?req=".
                    urlencode(trim(implode("/",str_replace("/","",$new_request)))).
                    "'>".
                    $dir.
                    "</a></div>";
                    }
                }else{
                    if(100 * ($page) <= $counter && $counter < 100 * ($page+1)){
                    $files_html_element[] = "<div class='col-3 col-md-4 col-sm-12' style='margin:10px auto;'><a style='background-size: 100%;background:url(\"".(str_replace("/opt/bitnami/apache/htdocs","",($path."/".$dir)))."\");' href='".
                    "?req=".
                    urlencode(trim(implode("/",str_replace("/","",$new_request)))).
                    "'><span style='background-color:rgba(255,255,255,.9);'>".
                    $dir.
                    "</span></a></div>";
                    }
                    $counter++;
                }
            }
        }
        echo "</div>";
        echo "
        <ul class='list-group'><li class='list-group-item'>ファイル</li></ul><div class='mt-4 container-fluid row'>";
        foreach($files_html_element as $element){
            echo $element;
        }
        echo "</div>";
        echo "<div class='my-4 container-fluid row'>";
        if($page > 0){
            echo "<div class='col-6'><a href='".
            "?p=".($page+1)."&req=".
            urlencode(trim(implode("/",str_replace("/","",$request)))).
            "' style='text-align:center;padding:40px auto;'>前へ</a></div>";
        }else{
            
            echo "<div class='col-6'><a style='text-align:center;padding:40px auto;'>戻れません</a></div>";
        }
        echo "<div class='col-6'><a href='".
        "?p=".($page+1)."&req=".
        urlencode(trim(implode("/",str_replace("/","",$request)))).
        "' style='text-align:center;padding:40px auto;'>次へ</a></div>";
        echo "</div>";
    };
?>
<ul class="list-group">
<li class='list-group-item'><?= $path ?></li>
</ul>
<?php if(!is_file($path)): ?>

<?php $printFunc($dirs); ?>
<?php else: ?>
<ul class="list-group">
    <li class='list-group-item'>ファイル情報</li>
    
<?php
echo "<li class='list-group-item'><a href='".str_replace("/opt/bitnami/apache/htdocs","",$path)."'>このファイルを閲覧する</a></li>";

$new_request = $request;
$new_request[] = "..";
echo "<li class='list-group-item'><a href='javascript:history.go(-1)'>戻る</a></li>";

if(substr($path,-4) === ".mp4"):
<?php endif; ?>
</ul>
<?php endif; ?>
</body>
</html>